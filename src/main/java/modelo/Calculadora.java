/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;



/**
 *
 * @author mlara
 */
public class Calculadora {
    
    private int monto;
    private int annos;
    private double interes = 0.1;
    private int resultado;

    /**
     * @return the monto
     */
    public int getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(int monto) {
        this.monto = monto;
    }

    /**
     * @return the annos
     */
    public int getAnnos() {
        return annos;
    }

    /**
     * @param annos the annos to set
     */
    public void setAnnos(int annos) {
        this.annos = annos;
    }

    
    /**
     * @return the resultado
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
    
    
    public double calculo (){
        return  this.getMonto() * interes * this.getAnnos();
    
    
    }
    public double calculo (int monto, int annos){
        this.setMonto(monto);
        this.setAnnos(annos);
        
        return calculo();
    }


}
    
    
    




    
