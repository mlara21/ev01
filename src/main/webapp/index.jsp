<%-- 
    Document   : index
    Created on : 10-04-2021, 10:12:52
    Author     : mlara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de Interes Simple</title>
    </head>
    <body>
        <h1>Plataforma de Prestamo</h1>
        <form name="form" action="CalController" method="POST" >
            <p>Ingrese su Nombre</p>
            <input type="text" id="nombre" name="nombre">
            
            <p>Ingrese Monto a Solicitar</p>
            <input type="number" id="monto" name="monto">
            
            <p>Ingrese Plazo en Años</p>
            <input type="number" id="anno" name="annos">
            
            <p>Nuestro Banco tiene una tasa de interes de 10% anual</p>
                   
            <p><button type="submit" name="calcular" class="btn btn-success">Calcular</button></p>  
          
        
        </form>
    </body>
</html>
