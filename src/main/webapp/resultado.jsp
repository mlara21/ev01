<%-- 
    Document   : resultado
    Created on : 10-04-2021, 10:49:20
    Author     : mlara
--%>

<%@page import="modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    
    Calculadora cal=(Calculadora)request.getAttribute("calculadora"); 

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="POST" action="CalController" name="form">
            <h2>Estimado/a</h2>
            <label for="result">Interes a Pagar</label>
            <input name="result" value="<%=cal.calculo()%>" class="form-control">
            
            
        </form>
    </body>
</html>
